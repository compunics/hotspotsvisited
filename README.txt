Plugin: HotspotsVisited
Author: Michael Brüning <M.Bruening@compunics.com.ni

Update 01.05.2017:
Starting with version 3.0 of the plugin, the required funtion gets inserted automatically into the output HTML-file,
no need to manually insert it into the file or the template.

Caution:
This plugin requires a function "visitedSpot(<spot-id>, <imagefile>)" to be defined in your calling HTML-file,
which is NOT part of this plugin.

Example:
<script type="text/javascript">
function visitedSpot(spotName,spotImage)
{
	jQuery('#'+spotName+' p img').attr('src',spotImage);
}
</script>

